(function ($) {
  Drupal.behaviors.galleriacommerce = {
    attach: function (context, settings) {
      Galleria.ready(function(options) {
        this.bind('image', function(e) {
          var gallery = Galleria.get(0);
          var current = gallery.getData(gallery.getIndex());
          var fid = current.original.attributes.fid.value;
          $('input[name="line_item_fields[galleria_commerce_lineitem_imgid][und][0][value]"]').attr('value', fid);
          $('input[name="line_item_fields[galleria_commerce_lineitem_img][und][0][fid]"]').attr('value', fid);
        });
      });
      $('.field-name-galleria-commerce-lineitem-img').hide();
      $('.field-name-galleria-commerce-lineitem-imgid').hide();
    }
  };
}(jQuery));
