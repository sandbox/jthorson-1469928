Galleria Commerce Integration
=============================

Allows users to add an image from a Galleria nodereference gallery to a Drupal
Commerce Cart, using a custom line item.

Notes
=====
- Ensure your Galleria module version can support a basic nodereference image
  gallery before getting started.  At time of release, the latest 7.x-1.x
  Galleria version appeared to be broken.
- The module supports displaying multiple 'add to cart' forms with a single
  nodereference gallery, and each will be dynamically updated as long as they
  contain the required line item fields.

Getting Started
===============

1. Download and enable prerequisite modules.

2. Apply the Galleria patches included in the Galleria_Commerce project
   directory.

3. Enable the Galleria_Commerce module.
   - The module will create the following items on your site.  You may use the
     default items, or delete them after using them as a reference to create
     your own.

     a) Content Types
        i. Galleria Commerce Image (galleria_commerce_image)
            Represents the images which will appear in the Galleria gallery.
       ii. Galleria Commerce Gallery (galleria_commerce_gallery)
            Represents the nodereference node which will contain the gallery.
            Contains a product reference field, which in turn is configured
	    to use the 'add to cart' display widget.  The display widget is
	    configured to use the 'Galleria Image' line item type.

     b) Commerce Line Item Types
        i. Galleria Image (galleria_commerce_image)
            Line item used to add a galleria image to a cart.  Contains two
            custom fields which are essential to the operation of the module
            - Galleria Image File (galleria_commerce_lineitem_img)
            - Galleria Image File ID (galleria_commerce_lineitem_imgid)
                - These fields could instead be added to the default product
                  line item, instead of using a custom line item type.

     c) Commerce Product Types
        i. Galleria Commerce Image
            A default Galleria Commerce product type

     d) Commerce Product
        i. Galleria Commerce Image
            A default Galleria Commerce product

4. Visit /admin/structure/types/manage/galleria-commerce-gallery/display, click
   on the operations icon for the Gallery Images field, click 'update' to
   detect the appropriate image field, and click 'Save' to commit the changes.

   NOTE:
   Failure to do this step will result in 'The referenced field does not
   contain any image field' warnings, and a 'load failed: no data found'
   message in place of the gallery.

5. Visit /admin/commerce/config/line-items/galleria-commerce-image/fields,
   edit the 'Galleria Image File' and 'Galleria Image File ID' fields, and
   check the 'Include this field on Add to Cart forms for line items of this
   type' checkbox for each.  Be sure to save the settings once complete.

   NOTE: If someone knows how to enable these checkboxes programatically,
   please let me know!

6. Visit /admin/structure/views/view/commerce_cart_form/edit/default and
   add the 'Galleria Image File' and 'Galleria Image File ID' fields to the
   shopping cart view.  Choose 'thumbnail' for the display type on the image
   field.  Ideally, reorder the view to display the image and image id fields
   in the first two columns of the view.

7. Create a number of 'Galleria Commerce Image' nodes.

8. Create a 'Galleria Commerce Gallery' node, referencing the nodes created in
   step #3.

9. When displaying both the Galleria nodereference gallery and Add to Cart
   form on the same page, the add to cart form will now update the line item
   fields with the fid of the current image being displayed within the gallery
   when items are added to the cart.


TODOs
=====
- Javascript required, but currently is not validated.

